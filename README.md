# MNST Bank
![로고](https://gitlab.com/sesac-sk1/site1/-/raw/main/logo.png)

## 소개

이 프로젝트는 Java 11과 Spring Boot를 사용하여 개발되었습니다.
CentOS 7 환경에서 MySQL 데이터베이스와 Apache Tomcat 서버를 통해 실행됩니다.

## 개발 환경

### 언어 및 프레임워크
- **Java 11**
- **Spring Boot 2.7.15**

### 운영 체제
- **CentOS 7**

### 데이터베이스
- **MySQL 5.7.44**

### 서버
- **Tomcat 8.0.53**
- **Apache 2.2.3**

### 클라우드 환경
![클라우드 아키텍처](https://gitlab.com/sesac-sk1/site1/-/raw/main/cloud.png)

## 설치 및 실행

### 필수 조건

1. Java 11이 설치
2. MySQL 5.7.44 데이터베이스가 설치
3. Apache Tomcat 8.0.53 서버가 설치

### 설치 방법

1. ROOT.war 파일을 톰캣의 webapps 내부폴더로 이동
2. Apache Tomcat의 conf/server.xml 파일에서 ROOT.war 파일을 참조하도록 내용 변경
